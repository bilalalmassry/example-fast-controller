<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Http\Requests\StoreCategoryRequest;
use App\Http\Requests\UpdateCategoryRequest;

class CategoryController extends BaseController
{
    public function __construct()
    {
        parent::__construct(Category::class,
            StoreCategoryRequest::class,
            UpdateCategoryRequest::class);
    }
}
