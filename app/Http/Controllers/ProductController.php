<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreProductRequest;
use App\Http\Requests\UpdateProductRequest;
use App\Models\Product;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;

class ProductController extends BaseController
{

    public function __construct()
    {
        parent::__construct(Product::class,
                          StoreProductRequest::class,
                         UpdateProductRequest::class);
    }

}
