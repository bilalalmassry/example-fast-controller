<?php


namespace App\Http\Controllers;


use App\Models\Product;
use Faker\Provider\Base;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use App\Models\BaseModel;
use ReflectionClass;

class BaseController extends Controller
{

    protected $className;
    protected $storeRequest;
    protected $updateRequest;


    public function __construct($className, $storeRequest, $updateRequest)
    {
        $this->className     = $className;
        $this->storeRequest  = $storeRequest;
        $this->updateRequest = $updateRequest;

    }
    /**
     * Display a listing of the resource.
     *
     * @return JsonResponse
     */
    public function index(): JsonResponse
    {
        return response()->json($this->className::all());
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param Request $request
     * @return JsonResponse
     */
    public function store(Request $request): JsonResponse
    {
        $request = app($this->storeRequest);
        return response()->json(['data' => $this->className::create($request->all())]);
    }

    /**
     * Display the specified resource.
     *
     * @param $id
     * @return JsonResponse
     */

    public function show($id): JsonResponse
    {
        $modelInstance = new $this->className();
        return response()->json(['data' =>  $modelInstance->findOrFail($id)]);;
    }

    /**
     * Update the specified resource in storage.
     *
     * @param Request $request
     * @param $id
     * @return JsonResponse
     */
    public function update(Request $request,  $id): JsonResponse
    {
        $request = app($this->updateRequest);
        $modelObject = $this->className::FindOrFail($id);
        $modelObject->update($request->all());
        return response()->json(['data' => $modelObject]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param $id
     * @return JsonResponse
     */
    public function destroy($id): JsonResponse
    {
        $modelInstance = new $this->className();
        return response()->json($modelInstance->findOrFail($id)->delete());
    }
}
















//        $reflector = new ReflectionClass(Product::class);
//        dd($reflector);
//        $a  = $id ($this->className);
//        dd($a);
//        ${$this->varName} = $id;
//        dd(${$this->varName});
//        dd($product->resolveChildRouteBinding('Product','1','product' ));
//
//        dd(${''.strtolower('product')});
//        $baseModel = new $this->className();
////        dd(get_class($baseModel));
//        return  $baseModel;
