<?php

namespace App\Http\Controllers;

use App\Http\Requests\StoreMarketRequest;
use App\Http\Requests\UpdateMarketRequest;
use App\Models\Market;

class MarketController extends BaseController
{
    public function __construct()
    {
        parent::__construct(Market::class,
            StoreMarketRequest::class,
            UpdateMarketRequest::class);
    }
}
