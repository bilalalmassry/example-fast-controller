<?php


namespace App\Models;


use Illuminate\Database\Eloquent\Model;

class BaseModel extends Model
{
    //
    public function resolveRouteBinding($value, $field = null): ?Model
    {
        return $this->where('id', $value)->firstOrFail();
    }
}
